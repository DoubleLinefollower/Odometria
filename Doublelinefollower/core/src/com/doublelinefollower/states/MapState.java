package com.doublelinefollower.states;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.doublelinefollower.Doublelinefollower;
import com.doublelinefollower.objects.Robocik;
import com.doublelinefollower.tcpip.Server;

import java.util.logging.Handler;

/**
 * Created by Szczypiorek on 19.03.2017.
 */

public class MapState extends State {

    private Texture bg;
    private Robocik robocik;
    private Vector2 vectorAdder;
    private int fromClient;
    public boolean clientSentSomething;
    private Thread serverThread;

    public MapState(StateManager sm, int port) {
        super(sm);
        fromClient = 0;
        cam.setToOrtho(false, Doublelinefollower.WIDTH / 2, Doublelinefollower.HEIGHT / 2);
        System.out.println("Port przekabacony do MapState: " + port);

        Pixmap pixmap200 = new Pixmap(Gdx.files.classpath("map.jpg"));
        Pixmap pixmap100 = new Pixmap(Doublelinefollower.WIDTH / 2, Doublelinefollower.HEIGHT / 2, pixmap200.getFormat());
        pixmap100.drawPixmap(pixmap200,
                0, 0, pixmap200.getWidth(), pixmap200.getHeight(),
                0, 0, pixmap100.getWidth(), pixmap100.getHeight()
        );
        bg = new Texture(pixmap100);

        robocik = new Robocik();
        robocik.setPosition(new Vector2(400, 400));
        vectorAdder = new Vector2();
/*
        try {
            server = new Server(port);
        } catch (Exception e) {
            e.printStackTrace();
        }*/

        clientSentSomething = false;

        try {
            serverThread = new Thread(new ServerThreading(port));
            serverThread.start();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    protected void handleInput() {

        if(clientSentSomething==true){
            clientSentSomething=false;
            vectorAdder = new Vector2(0, fromClient);
            robocik.update(vectorAdder);
            vectorAdder = new Vector2(0,0);
            System.out.println("dodalem "+fromClient);
        }

    }

    @Override
    public void update(float dt) {
        handleInput();
        robocik.update(vectorAdder);
        vectorAdder = new Vector2(0, 0);
        cam.update();
    }

    @Override
    public void render(SpriteBatch sb) {
        sb.setProjectionMatrix(cam.combined);
        sb.begin();
        sb.draw(bg, 0, 0);
        sb.draw(robocik.getRobotTex(), robocik.getPosition().x, robocik.getPosition().y);
        sb.end();
    }

    @Override
    public void dispose() {
        bg.dispose();
    }

    public class ServerThreading implements Runnable {
        public Server server;
        private int tmp;
        private int port;
        
        public ServerThreading(int port) {
            this.port = port;
        }

        @Override
        public void run() {
        	try {
                server = new Server(port);
            } catch (Exception e) {
                e.printStackTrace();
            }
        	while(true){
        		System.out.println("Runnable, czekam na fromClient");
	            tmp = server.receiveFromClient();
	            handleServerInput(tmp);
        		System.out.println("Runnable, zhandlowalem");

        	}
        	}
    }

    //poki co przesylamy int, czy ma jechac do gory czy na dol (+/-)
    public void handleServerInput(int clientInfo) {
    	System.out.println("HANDLERUJE "+clientInfo);
        clientSentSomething = true;
        fromClient = clientInfo;
    }

}
