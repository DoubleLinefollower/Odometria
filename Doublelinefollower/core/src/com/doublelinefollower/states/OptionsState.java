package com.doublelinefollower.states;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextArea;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.utils.Align;
import com.doublelinefollower.Doublelinefollower;

import java.awt.Dimension;
import java.awt.Toolkit;

/**
 * Created by Szczypiorek on 19.03.2017.
 */

public class OptionsState extends State {

    private static final String PORT_STRING = "Port";
    private static final int FONT_SIZE = 15;

    private Texture bg;
    private Texture portText;
    private TextArea port;
    private Stage stage;
    private Skin skin;
    private BitmapFont portFont;
    private String portInput;
    private int portInputInteger;
    private TextButton portButton;
    private boolean permission;
    Animation benAnim, benAnimRev;
    private float frameCounter;
    private boolean bul;
    private int counter;
    private Sound tiruRiru;


    public OptionsState(StateManager sm, Animation anim, Animation animRev) {
        super(sm);
        
        stage = new Stage();
        bg = new Texture("bg5.jpg");
        portText = new Texture("portText.png");

        Pixmap pixmap200 = new Pixmap(Gdx.files.classpath("portText.png"));
        Pixmap pixmap100 = new Pixmap(1000, 250, pixmap200.getFormat());
        pixmap100.drawPixmap(pixmap200,
                0, 0, pixmap200.getWidth(), pixmap200.getHeight(),
                0, 0, pixmap100.getWidth(), pixmap100.getHeight()
        );
        portText = new Texture(pixmap100);

        cam.setToOrtho(false, Doublelinefollower.WIDTH, Doublelinefollower.HEIGHT);

        portInput = null;
        permission = true;
        	
        counter=0;
        
        Toolkit toolkit = Toolkit.getDefaultToolkit();
        Dimension dim = toolkit.getScreenSize();

        skin = new Skin(Gdx.files.classpath("rainbow-ui.json"));
        port = new TextArea(PORT_STRING, skin);
        port.setWidth(Math.round(dim.getWidth()/6));
        port.setHeight(Math.round(dim.getHeight()/10));
        port.setX(Doublelinefollower.WIDTH / 2 - port.getWidth()/2);
        port.setY(Doublelinefollower.HEIGHT /2);
        port.setAlignment(Align.center);
        port.setText("Port");
        
        frameCounter=0;
        benAnim = anim;
        benAnimRev = animRev;
        //benAnim = com.holidaystudios.tools.GifDecoder.loadGIFAnimation(1, Gdx.files.classpath("resized.gif").read());
        //benAnimRev = com.holidaystudios.tools.GifDecoder.loadGIFAnimation(1, Gdx.files.classpath("benRev.gif").read());

        Gdx.input.setInputProcessor(stage);

        portButton = new TextButton("Akceptacja sytuacji", skin);
        portButton.setWidth(650);
        portButton.setHeight(85);
        portButton.setX(Doublelinefollower.WIDTH / 2 - portButton.getWidth() / 2);
        portButton.setY(Doublelinefollower.HEIGHT / 2 - portButton.getHeight()*5/3 );

        stage.addActor(port);
        stage.addActor(portButton);
        	
        bul=false;
        
        tiruRiru = Gdx.audio.newSound(Gdx.files.classpath("tiruRiru.wav"));
        
    }

    @Override
    protected void handleInput() {
        if (portButton.isPressed()) {
            permission = true;
            portInput = port.getText();
            try {
                portInputInteger = Integer.valueOf(portInput);
            } catch (NumberFormatException e) {
                e.printStackTrace();
                port.setText("Wpisano bledny numer portu");
                permission = false;
            }
            if (permission == true) {
                sm.push(new MapState(sm, portInputInteger));
            }
            else if(permission == false){
                port.setText("Wpisano bledny numer portu");
            }
        }
    }

    @Override
    public void update(float dt) {
        handleInput();
    }

    @Override
    public void render(SpriteBatch sb) {
        sb.setProjectionMatrix(cam.combined);
        sb = (SpriteBatch) stage.getBatch();
        sb.begin();
        sb.draw(bg, 0, 0);
        sb.draw(portText, cam.position.x-portText.getWidth()/2, cam.position.y+portText.getHeight()/2);
		frameCounter += Gdx.graphics.getDeltaTime();
		counter++;
		if(counter==330){
			if(bul==true)
				bul = false;
			else if(bul==false)
				bul=true;
			counter=0;
			tiruRiru.play();
		}
		if(bul==false)
			sb.draw((TextureRegion) benAnim.getKeyFrame(frameCounter, true), 350,-100);
		else if(bul==true)
			sb.draw((TextureRegion) benAnimRev.getKeyFrame(frameCounter, true), 350,-100);
        sb.end();
        stage.draw();
    }

    @Override
    public void dispose() {
        portFont.dispose();
        bg.dispose();
    }
}
