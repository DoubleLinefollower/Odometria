package com.doublelinefollower.states;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.util.Timer;
import java.util.TimerTask;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.doublelinefollower.Doublelinefollower;

/**
 * Created by Szczypiorek on 19.03.2017.
 */

public class MenuState extends State {

	private static final int ANIMATION_SIZE = 100;

	private Texture titleTex;
	private Texture bgTex;
	private Texture teamTex;
	private Dimension dim;
	private float frameCounter;
	private Animation anim, benAnim, benAnim2;
	private Sound benSound1, benSound2;
	private Timer t;

	private int sizeX, sizeY, counter, counterTeam, mainSizeX, mainSizeY, mainTeamSizeX, mainTeamSizeY;
	boolean direction;

	public MenuState(StateManager sm) {
		super(sm);
		cam.setToOrtho(false, Doublelinefollower.WIDTH, Doublelinefollower.HEIGHT);
		titleTex = new Texture("title.png");

		bgTex = new Texture("bg5.jpg");

		Toolkit toolkit = Toolkit.getDefaultToolkit();
		dim = toolkit.getScreenSize();

		teamTex = new Texture("ekipa2.png");
		mainTeamSizeX = teamTex.getWidth() * 2;
		mainTeamSizeY = teamTex.getHeight() * 2;

		Pixmap pixmap200 = new Pixmap(Gdx.files.classpath("ekipa4.png"));
		Pixmap pixmap100 = new Pixmap(mainTeamSizeX, mainTeamSizeY, pixmap200.getFormat());
		pixmap100.drawPixmap(pixmap200, 0, 0, pixmap200.getWidth(), pixmap200.getHeight(), 0, 0, pixmap100.getWidth(),
				pixmap100.getHeight());

		teamTex = new Texture(pixmap100);
		mainSizeX = 1720;
		mainSizeY = 200;
		counter = 1;
		counterTeam = 1;
		direction = true;

		frameCounter = 0;
		
		TimerTask tt = new TimerTask() {
			@Override
			public void run() {
				double rand;
				rand=Math.random()*1;
				if (Math.round(rand) == 1) {
					benSound1.play();
			    } else {
			    	benSound2.play();
			    }
			}
		};
		
		t = new Timer();
		t.scheduleAtFixedRate(tt, 5500, 2000);
		
		benSound1 = Gdx.audio.newSound(Gdx.files.classpath("ben1.wav"));
		benSound2 = Gdx.audio.newSound(Gdx.files.classpath("ben2.wav"));

		anim = com.holidaystudios.tools.GifDecoder.loadGIFAnimation(1, Gdx.files.classpath("lopata.gif").read());
		benAnim = com.holidaystudios.tools.GifDecoder.loadGIFAnimation(1, Gdx.files.classpath("resized.gif").read());
		benAnim2 = com.holidaystudios.tools.GifDecoder.loadGIFAnimation(1, Gdx.files.classpath("resizedRev.gif").read());

	}

	@Override
	protected void handleInput() {
		if (Gdx.input.justTouched()) {
			t.cancel();
			sm.push(new OptionsState(sm, benAnim, benAnim2));
		}
	}

	@Override
	public void update(float dt) {

		sizeX = counter * mainSizeX / 200;
		sizeY = counter * mainSizeY / 200;

		Pixmap pixmap200 = new Pixmap(Gdx.files.classpath("title.png"));
		Pixmap pixmap100 = new Pixmap(sizeX, sizeY, pixmap200.getFormat());
		pixmap100.drawPixmap(pixmap200, 0, 0, pixmap200.getWidth(), pixmap200.getHeight(), 0, 0, pixmap100.getWidth(),
				pixmap100.getHeight());
		titleTex = new Texture(pixmap100);

		if (counter >= 65 * 2) {
			if (direction == true)
				counter += 1;
			if (direction == false) {
				counter -= 1;
				if (counter == 40)
					direction = true;
			}
			if (counter > ANIMATION_SIZE * 2)
				direction = false;
		} else if (counter < 65 * 2) {
			if (direction == true)
				counter += 2;
			if (direction == false) {
				counter -= 2;
				if (counter == 40 || counter == 19)
					direction = true;
			}
			if (counter > ANIMATION_SIZE * 2)
				direction = false;
		}

		handleInput();

	}

	@Override
	public void render(SpriteBatch sb) {
		sb.setProjectionMatrix(cam.combined);
		sb.begin();
		sb.draw(bgTex, 0, 0);
		sb.draw(teamTex, cam.position.x - teamTex.getWidth() / 2,
				cam.position.y - teamTex.getHeight() / 2 - teamTex.getHeight() / 8);
		//sb.draw(titleTex, cam.position.x - titleTex.getWidth() / 2, cam.position.y + titleTex.getHeight());
		frameCounter += Gdx.graphics.getDeltaTime();
	
		sb.draw((TextureRegion) anim.getKeyFrame(frameCounter, true), cam.position.x - teamTex.getWidth() / 2-100,
				cam.position.y - teamTex.getHeight()/2-120);
		sb.draw(titleTex, cam.position.x - titleTex.getWidth() / 2, cam.position.y + titleTex.getHeight());
		sb.end();
	}

	@Override
	public void dispose() {
		titleTex.dispose();
		bgTex.dispose();
		teamTex.dispose();
		dispose();
		
	}
}
