package com.doublelinefollower.tcpip;


import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * Created by Szczypiorek on 19.03.2017.
 */

public class Server {

    private int port;
    private String fromClient;
    private ServerSocket server;
    private Socket client;
    private BufferedReader in;
    private PrintWriter out;


    public Server(int port) throws Exception {
        server = new ServerSocket(port);
        System.out.println("wait for connection on port 8080");
        client = server.accept();
        in = new BufferedReader(new InputStreamReader(client.getInputStream()));
        out = new PrintWriter(client.getOutputStream(), true);
        System.out.println("got connection on port " + port);
    }


    public int receiveFromClient() {
        try {
            fromClient = in.readLine();
    
            System.out.println("received: " + fromClient);
            if(fromClient!=null){
            try {
                return Integer.valueOf(fromClient);
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }}
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

}
