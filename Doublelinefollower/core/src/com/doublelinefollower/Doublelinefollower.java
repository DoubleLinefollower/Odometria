package com.doublelinefollower;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.doublelinefollower.states.MenuState;
import com.doublelinefollower.states.StateManager;

import java.awt.Dimension;
import java.awt.Toolkit;

public class Doublelinefollower extends ApplicationAdapter {

    public static int WIDTH;
    public static int HEIGHT;
    public static final String TITLE = "Doublelinefollower";

    private StateManager sm;
    SpriteBatch batch;
    public Music music;


    @Override
    public void create() {
        Toolkit toolkit = Toolkit.getDefaultToolkit();
        Dimension dim = toolkit.getScreenSize();
        WIDTH = dim.width;
        HEIGHT = dim.height;
        System.out.println(WIDTH+", "+HEIGHT);
        music = Gdx.audio.newMusic(Gdx.files.classpath("music.wav"));
        music.setLooping(true);
        music.play();

        batch = new SpriteBatch();
        sm = new StateManager();
        sm.push(new MenuState(sm));
    }

    @Override
    public void render() {
        Gdx.gl.glClearColor(1, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        sm.update(Gdx.graphics.getDeltaTime());
        sm.render(batch);
    }

    @Override
    public void dispose() {
        batch.dispose();
    }
}


