package com.doublelinefollower.objects;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Vector2;

/**
 * Created by Szczypiorek on 19.03.2017.
 */

public class Robocik {

    private static final int SIZE = 64;

    private Texture robotTex;
    private Vector2 position;

    public Robocik() {
        Pixmap pixmap200 = new Pixmap(Gdx.files.classpath("robot.png"));
        Pixmap pixmap100 = new Pixmap(SIZE, SIZE, pixmap200.getFormat());
        pixmap100.drawPixmap(pixmap200,
                0, 0, pixmap200.getWidth(), pixmap200.getHeight(),
                0, 0, pixmap100.getWidth(), pixmap100.getHeight()
        );

        robotTex = new Texture(pixmap100);

    }

    public Texture getRobotTex() {
        return robotTex;
    }

    public void setRobotTex(Texture robotTex) {
        this.robotTex = robotTex;
    }

    public Vector2 getPosition() {
        return position;
    }

    public void setPosition(Vector2 position) {
        this.position = position;
    }

    public void update(Vector2 vecAdd ){
        position.add(vecAdd);
    }

}
