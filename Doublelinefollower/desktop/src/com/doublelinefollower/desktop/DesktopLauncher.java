package com.doublelinefollower.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.doublelinefollower.Doublelinefollower;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		new LwjglApplication(new Doublelinefollower(), config);
		config.width = Doublelinefollower.WIDTH;
		config.height = Doublelinefollower.HEIGHT;
		config.title = Doublelinefollower.TITLE;
	}
}
