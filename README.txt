W ga��zi enkodery tworzymy kod, kt�ry zczytuje dane z enkoder�w zamontowanych na silnikach naszego robota i przesy�a informacje
do kontrolera arduino za pomoc� komunikacji I2C. Informacje do arduino s� przesy�ane jako String, kt�rego szablon prezentuje sie nast�puj�co:

11.1122.2233.3344.440100

gdzie 11.11 to pr�dko�� ko�a pierwszego, 22.22 pr�dko�� ko�a drugiego, 33.33 pr�dko�� ko�a trzeciego, 44.44 pr�dko�� ko�a czwartego, 
za� 0100 to warto�� skr�tu jaki powinien robot wykona�. Warto�� t� b�dziemy otrzymywa� od grupy zajmuj�cej si� przetwarzaniem obrazu z kamery, kt�ra
na bazie linii widocznych w kamerze b�dzie decydowa� w jakim kierunku b�dzie porusza� si� robot. Kierunek jazdy robota b�dzie determinowany warto�ciami od -100 do 100, 
tak wi�c "0100" na ko�cu naszego stringa reprezentuje liczb� od 0 do 100, za� pierwsza liczba to "bit" znaku. 0 reprezentuje -, za� 1 reprezentuje +.



Aby komunikacja I2C zadzia�a�a, nale�y pod��czy� uziemienie z raspberry do uziemienia arduino, a nast�pnie pod��czy� Pin nr 2 (SDA) w raspberry
do pinu PC4 I2C SDA w arduino, za� pin  3 (SCL) do pinu PC5 I2C SCL w arduino.

Aby zczytywanie z enkoder�w zadzia�a�o nale�y pod��czy� wszystkie enkodery w nast�puj�cy spos�b: 
Czerwone kable do napi�cia 3.3V w raspberry
Czarne kable do uziemienia w raspberry
Bia�e kable przez rezystor 22kOhm do pin�w: 4, 14, 15 ,18 

Nast�pnie nale�y uruchomi� arduino i kod zrobiony przez grup� steruj�c� silnikami, a potem uruchomi� kod na raspberry.
Gotowe, kod b�dzie przesy�a� co sekund� informacje na temat pr�dko�ci k� wyliczonej za pomoc� enkoder�w. 