#
#	Mapa kol z ich numerami z tablic i pinami
#
#  Numery: 
#          ^
#		(3)   (2)
#		(1)__(0)
#
#	Piny:
#            ^
#	   (15)     (14)
#		(18)____(4)

from gpiozero import Button
import time 
import numpy as np
import smbus
i = []
wyslany_string=""
srednicaKola = 6.5

czas_odswiezania = 1
enkoder=[]


def writeNumber2(value):
    bus = smbus.SMBus(1)
    address = 0x04
    bus.write_byte(address, value)

def writeNumber(val):
        
    bus = smbus.SMBus(1)
    address = 0x04
    for character in str(val): # convert into a string and iterate over it
            bus.write_byte(address, ord(character)) # send each char's ASCII encoding

def liczeniePredkosci(p, k):
    
    global srednicaKola
    global czas_odswiezania
    
    odleglosc = srednicaKola/8*p*3.14
    predkosc = odleglosc/czas_odswiezania
    predkosc2 = ("{:1.2f}".format(predkosc))
    if(predkosc<10):
        predkosc2="0"+predkosc2
    return predkosc2

def licznikPraweTyl():
    global i
    i[0] = i[0] + 1
	
def licznikLeweTyl():
    global i
    i[1] = i[1] + 1
	
def licznikPrawePrzod():
    global i
    i[2] = i[2] + 1
	
def licznikLewePrzod():
    global i
    i[3] = i[3] + 1
    
for k in range(0,4):
	i.append(0)



enkoder.append(Button(4))
enkoder.append(Button(18))
enkoder.append(Button(14))
enkoder.append(Button(15))

enkoder[0].when_pressed = licznikPraweTyl
enkoder[1].when_pressed = licznikLeweTyl
enkoder[2].when_pressed = licznikPrawePrzod
enkoder[3].when_pressed =  licznikLewePrzod



while(1):
    time.sleep(czas_odswiezania)
    wysylany_string=""
    for k in range(0,4):
        p = i[k]
        wysylany_string+=str((liczeniePredkosci(p, k)))
        i[k]=0
    wysylany_string+="0100"
    writeNumber(wysylany_string)
    print("wysylany string: ", wysylany_string)